 // Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

void AFood::SetFoodType()
{
	int a = rand() % (10 + 10 + 1) - 10;
	if (a >= 0)
	{
		FastFood = true;
		SetFoodFastTypeMaterial();
	}
	else
	{
		FastFood = false;
		SetFoodSlowTypeMaterial();
	}

}

void AFood::SetFoodFastTypeMaterial_Implementation()
{

}

void AFood::SetFoodSlowTypeMaterial_Implementation()
{

}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			if ((FastFood == false) && (Snake->MovementSpeed >= 5.f))
			{
				Snake->MovementSpeed = Snake->MovementSpeed - 5.f;
			}
			else if ((FastFood == true) && (Snake->MovementSpeed <= 100.f))
			{
				Snake->MovementSpeed = Snake->MovementSpeed + 5.f;
			}

			Snake->AddFood();
			Destroy();
		}
	}
}

