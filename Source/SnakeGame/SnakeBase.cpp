// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Food.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::LEFT;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(1);
	AddFood();
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for(int i = 0; i < ElementsNum; ++i)
	{ 
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElement);
		if (ElemIndex == 0) 
		{
			NewSnakeElement->SetFirstElementType();
			NewSnakeElement->MeshComponent->SetVisibility(true);
		}
		BodyContainer = NewSnakeElement;
		GetWorld()->GetTimerManager().SetTimer(BeforeBodySpawnTimer, this, &ASnakeBase::DelayBeforeVisible, 0.5f, false);
	}
}

void ASnakeBase::AddFood()
{
	int x, y;
	x = rand() % (470 + 470 + 1) - 470;
	y = rand() % (470 + 470 + 1) - 470;
	FVector RandomLocation(x, y, -10);
	FTransform NewTransform(RandomLocation);
	AFood* NewFood = GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
	NewFood->SetFoodType();

}

void ASnakeBase::DelayBeforeVisible()
{
	BodyContainer->MeshComponent->SetVisibility(true);
	GetWorldTimerManager().ClearTimer(BeforeBodySpawnTimer);
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	float MovementSpeedVector = ElementSize;

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += MovementSpeedVector;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= MovementSpeedVector;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y += MovementSpeedVector;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y -= MovementSpeedVector;
		break;
	}

	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	
	for (int i = SnakeElements.Num() - 1; i>0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if(IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

